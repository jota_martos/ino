#include "DHT.h"

#define DHTTYPE DHT22
#define DHT_PIN 13
#define PIN_STATUS 7
#define PIN_HUM_YELLOW 2
#define PIN_HUM_RED 3
#define PIN_TEMP_YELLOW 4
#define PIN_TEMP_RED 5

boolean status = false;
int high = 75;
int low = 65;

DHT dht(DHT_PIN, DHTTYPE);

void setup()
{
        pinMode(DHT_PIN, INPUT);
        pinMode(PIN_STATUS, OUTPUT);
        pinMode(PIN_TEMP_YELLOW, OUTPUT);
        pinMode(PIN_TEMP_RED, OUTPUT);
        pinMode(PIN_HUM_YELLOW, OUTPUT);
        pinMode(PIN_HUM_RED, OUTPUT);
        digitalWrite(PIN_TEMP_YELLOW, LOW);
        digitalWrite(PIN_TEMP_RED, LOW);
        digitalWrite(PIN_HUM_YELLOW, LOW);
        digitalWrite(PIN_HUM_RED, LOW);
        digitalWrite(PIN_STATUS, LOW);
	Serial.begin(9600);
        dht.begin();
        digitalWrite(PIN_TEMP_YELLOW, HIGH);
        digitalWrite(PIN_TEMP_RED, HIGH);
        digitalWrite(PIN_HUM_YELLOW, HIGH);
        digitalWrite(PIN_HUM_RED, HIGH);
        delay(5000);
}

void loop()
{
        float humidity = dht.readHumidity();
        float temp = dht.readTemperature();
	Serial.print("Humedad: ");
	Serial.print(humidity, 1);
	Serial.print(" % - Temperatura: ");
	Serial.print(temp, 1);
	Serial.println(" C");
        if (temp < 20)  {
                digitalWrite(PIN_TEMP_YELLOW, LOW);
                digitalWrite(PIN_TEMP_RED, LOW);
        } else if (temp < 35) {
                digitalWrite(PIN_TEMP_YELLOW, HIGH);
                digitalWrite(PIN_TEMP_RED, LOW);
        } else {
                digitalWrite(PIN_TEMP_YELLOW, LOW);
                digitalWrite(PIN_TEMP_RED, HIGH);
        }
        
        if (humidity < low)  {
                digitalWrite(PIN_HUM_YELLOW, LOW);
                digitalWrite(PIN_HUM_RED, LOW);
                if (status) {
                        status = false;
                        digitalWrite(PIN_STATUS, LOW);
                }
        } else if (humidity < high) {
                digitalWrite(PIN_HUM_YELLOW, HIGH);
                digitalWrite(PIN_HUM_RED, LOW);
        } else {
                digitalWrite(PIN_HUM_YELLOW, LOW);
                digitalWrite(PIN_HUM_RED, HIGH);
                if (!status) {
                        status = true;
                        digitalWrite(PIN_STATUS, HIGH);
                }
        }
        delay(600000);
}
