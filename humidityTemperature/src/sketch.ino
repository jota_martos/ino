#include "dht.h"
#include "TM1637.h"

#define DHT11_PIN 2
#define TM_CLK 3
#define TM_DIO 4

//{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
//0~9,A,b,C,d,E,F

TM1637 tm1637(TM_CLK,TM_DIO);
dht DHT;

void setup()
{
	tm1637.init();
	tm1637.set(2); 
	Serial.begin(9600);
	delay(5000);
}

void loop()
{
	DHT.read11(DHT11_PIN);
	int temp = DHT.temperature;
	int humidity = DHT.humidity;
	int digitoneT = temp / 10;
	int digittwoT = temp % 10;
	int digitoneH = humidity / 10;
	int digittwoH = humidity % 10;
	Serial.print("Humedad: ");
	Serial.print(DHT.humidity, 0);
	Serial.print(" % - Temperatura: ");
	Serial.print(DHT.temperature, 0);
	Serial.println(" C");
	tm1637.display(1,digitoneT); 
	tm1637.display(2,digittwoT);
	tm1637.display(3,12);  // put a C at the end
	delay (15000);
	tm1637.display(1,-1); 
	tm1637.display(2,digitoneH);
	tm1637.display(3,digittwoH);
	delay(15000);
}
