#include "DHT.h"
#include "dht.h"
#include "TM1637.h"

#define DHTTYPE DHT11
#define DHT_PIN 0
#define PIN_STATUS 1
#define TM_DIO 2
#define TM_CLK 3

//{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
//0~9,A,b,C,d,E,F

TM1637 tm1637(TM_CLK,TM_DIO);
DHT dht(DHT_PIN, DHTTYPE);

boolean status = false;
int high = 65;
int low = 55;
int tempMax = 50;
int humidity;
int temp;
int digitoneT;
int digittwoT;
int digitoneH;
int digittwoH;

void setup()
{
        pinMode(DHT_PIN, INPUT);
        pinMode(PIN_STATUS, OUTPUT);
        pinMode(TM_CLK, OUTPUT);
        pinMode(TM_DIO, OUTPUT);
        digitalWrite(PIN_STATUS, LOW);
        tm1637.init();
        tm1637.set(2);
        dht.begin();
        digitalWrite(PIN_STATUS, HIGH);
        tm1637.display(0,8);
        tm1637.display(1,8);
        tm1637.display(2,8);
        tm1637.display(3,8);                
        delay(10000);
        tm1637.display(0,-1);
        tm1637.display(1,-1);
        tm1637.display(2,-1);
        tm1637.display(3,-1);        
        digitalWrite(PIN_STATUS, LOW);
}

void loop()
{   
        humidity = dht.readHumidity();
        temp = dht.readTemperature();
        digitoneT = temp / 10;
        digittwoT = temp % 10;
        digitoneH = humidity / 10;
        digittwoH = humidity % 10;
        
        if (humidity < low)  {
                status = false;
        } else if (humidity > high) {
                status = true;
        }
        if (temp > tempMax) {
                status = true;
        }
        if (status) {
                digitalWrite(PIN_STATUS, HIGH);
        } else {
                digitalWrite(PIN_STATUS, LOW);          
        }

        tm1637.display(1,-1);
        tm1637.display(2,digitoneH);
        tm1637.display(3,digittwoH);
        delay(150000);
        tm1637.display(1,digitoneT);
        tm1637.display(2,digittwoT);
        tm1637.display(3,12);  // put a C at the end
        delay(150000);
}

